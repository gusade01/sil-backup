<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTPengangkutanHasAreaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_pengangkutan_has_area', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('t_pengangkutan_id');
            $table->unsignedInteger('area_id');
            $table->tinyInteger('is_default');
            $table->decimal('volume');
            $table->time('jam');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_pengangkutan_has_area');
    }
}
