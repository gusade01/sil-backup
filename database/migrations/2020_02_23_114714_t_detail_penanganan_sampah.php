<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TDetailPenangananSampah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_detail_penanganan_sampah', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('penanganan_sampah_id');
            $table->bigInteger('jenis_sampah_id');
            $table->decimal('volume');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_detail_penanganan_sampah');
    }
}
