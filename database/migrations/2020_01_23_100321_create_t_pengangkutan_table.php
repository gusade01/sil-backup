<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTPengangkutanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_pengangkutan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('armada_id');
            $table->unsignedInteger('area_id');
            $table->dateTime('tanggal');
            $table->decimal('volume');
            $table->text('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_pengangkutan');
    }
}
