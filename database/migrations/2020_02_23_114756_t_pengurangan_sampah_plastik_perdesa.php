<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TPenguranganSampahPlastikPerdesa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_pengurangan_sampah_plastik_perdesa', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('kelurahan_id');
            $table->decimal('volume');
            $table->date('tanggal');
            $table->text('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_pengurangan_sampah_plastik_perdesa');
    }
}
