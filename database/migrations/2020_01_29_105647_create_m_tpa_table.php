<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMTpaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_tpa', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_tpa',255);
            $table->string('kode',255);
            $table->unsignedInteger('kecamatan_id');
            $table->unsignedInteger('kelurahan_id');
            $table->text('keterangan');
            $table->decimal('lat',12,9);
            $table->decimal('ing',12,9);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_tpa');
    }
}
