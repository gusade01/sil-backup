<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_area', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_area');
            $table->string('kode');
            $table->integer('kecamatan_id');
            $table->integer('kelurahan_id');
            $table->text('keterangan')->nullable();
            $table->decimal('lat', 12, 9);
            $table->decimal('lng', 12, 9);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('areas');
    }
}
