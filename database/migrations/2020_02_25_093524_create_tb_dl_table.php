<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbDlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_dl', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->year('tahun');
            $table->integer('bulan');
            $table->char('dokumen',255);
            $table->text('nama_dokumen');
            $table->text('ket_dokumen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_dl');
    }
}
