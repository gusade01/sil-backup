<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTPengangkutanRelationAreaAndTpa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_pengangkutan', function (Blueprint $table) {
            $table->dropColumn('area_id');
            $table->dropColumn('volume');
            $table->string('bukti_foto',255);
            $table->unsignedBigInteger('tpa_id');
            $table->tinyInteger('done');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_pengangkutan', function (Blueprint $table) {
            $table->unsignedInteger('area_id');
            $table->decimal('volume');
            $table->dropColumn('bukti_foto');
            $table->dropColumn('tpa_id');
            $table->dropColumn('done');
        });
    }
}
