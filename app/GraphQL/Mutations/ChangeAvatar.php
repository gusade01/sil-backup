<?php

namespace App\GraphQL\Mutations;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use App\User;
use Exception;

class ChangeAvatar
{
    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args The arguments that were passed into the field.
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context Arbitrary data that is shared between all fields of a single query.
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): ?User
    {
        // TODO implement the resolver
        $file = $args['file'];
        $id = $args['id'];

        $user = User::find($id);

        try{
            if(!empty($file)){
                $folder_name = "avatar";
                $fileExtension = $file->getClientOriginalExtension();
                $filename = $folder_name."_".time().".".$fileExtension;
                $filePath = $file->storeAs('public/'.$folder_name , $filename);
                $user->avatar = $folder_name."/".$filename;
                $user->save();
            } else {
                
                $filePath = "Null";
            }
           
        } catch(Exception $e) {

            $user->avatar = $e->getMessage();
            $user->save();
            return $user;
        }

        return $user;
    }
}
