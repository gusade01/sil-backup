<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penanganan_Sampah extends Model
{
    protected $table = 't_penanganan_sampah';

    protected $fillable = [
        'tpa_id' , 'tanggal' , 'keterangan','sampah_terjual'
    ];

    public function detail_Penanganan_Sampahs()
    {
        return $this->hasMany('App\Detail_Penanganan_Sampah', 'penanganan_sampah_id');
    }
    
}
