<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'm_area';
    protected $fillable = ['nama_area', 'kode', 'kecamatan_id', 'kelurahan_id', 'keterangan', 'lat', 'lng'];

    public function armada()
    {
        return $this->belongsToMany('App\Armada', 'm_armada_has_area', 'area_id', 'armada_id');
    }

    public function pengangkutan()
    {
        return $this->hasMany('App\Pengangkutan', 'pengakutan_id');
    }

    public function kecamatan()
    {
        return $this->belongsTo('App\Kecamatan', 'kecamatan_id');
    }
    public function kelurahan()
    {
        return $this->belongsTo('App\Kelurahan', 'kelurahan_id');
    }
}
