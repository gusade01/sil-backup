<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tpst extends Model
{
    protected $table = 'm_tpst';
    protected $fillable = ['nama', 'kode', 'kecamatan_id', 'kelurahan_id', 'keterangan', 'lat', 'lng'];
}
