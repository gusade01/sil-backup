<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $table = 'm_kecamatan_new';
    protected $fillable = ['kabupaten_id', 'kecamatan'];

    public function kabupaten()
    {
    	return $this->belongsTo('App\Kabupaten');
    }
    
    public function kelurahans()
    {
        return $this->hasMany('App\Kelurahan', 'kecamatan_id');
    }

    public function area()
    {
        return $this->hasMany('App\Area', 'kecamatan_id');
    }
}
