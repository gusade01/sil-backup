<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    //
    protected $table = 'tb_kategori';

    // public function lingkungan()
    // {
    //     return $this->belongsToMany(DataLingkungan::class,'tb_detail_dl');
    // }

    public function lingkungan()
    {
        $pivotTable = config('admin.database.dl_kategori');

        $relatedModel = config('admin.database.dl_model');

        return $this->belongsToMany($relatedModel, $pivotTable, 'kategori_id', 'dl_id');
    }

    public $timestamps = true;


    protected $casts = ['kategori_id' => 'array'];
}
