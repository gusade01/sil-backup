<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lingkungan extends Model
{
    protected $table = 'm_lingkungan';
    protected $fillable = ['kelurahan_id', 'lingkungan'];
}
