<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    protected $table = 'm_kelurahan_new';
    protected $fillable = ['kecamatan_id', 'kelurahan'];

    public function kecamatan()
    {
        return $this->belongsTo('App\Kecamatan', 'kecamatan_id');
    }

    public function area()
    {
        return $this->hasMany('App\Area', 'kelurahan_id');
    }

    public function pengurangan_Sampah_Plastik_Perdesas()
    {
        return $this->hasMany('App\Pengurangan_Sampah_Plastik_Perdesa', 'kelurahan_id');
    }
}
