<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jenis_Sampah extends Model
{
    protected $table = "m_jenis_sampah";

    protected $fillable = [
        'jenis_sampah','keterangan'
    ];
}
