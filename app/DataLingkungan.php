<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataLingkungan extends Model
{
    //

    // protected $fillable = [
    //     'tahun', 'bulan', 'dokumen','ket_dokumen',
    // ];

    protected $table = 'tb_dl';

    // public function kategori()
    // {
    //     return $this->belongsToMany(Kategori::class, 'tb_detail_dl');
    // }

    public function kategori()
    {
        $pivotTable = config('admin.database.dl_kategori');

        $relatedModel = config('admin.database.kategori_model');

        return $this->belongsToMany($relatedModel, $pivotTable, 'dl_id', 'kategori_id');
    }

    // protected $dates = ['tahun','bulan'];

    public $timestamps = true;

    // protected $casts = ['kategori_id' => 'array'];
}

