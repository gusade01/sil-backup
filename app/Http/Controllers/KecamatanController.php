<?php

namespace App\Http\Controllers;

use App\Kecamatan;
use Illuminate\Http\Request;

class KecamatanController extends Controller
{
    public function kecamatan(Request $request){
        $q = $request->q;

        return Kecamatan::where('kecamatan', 'like', "%$q%")->paginate(null, ['id', 'kecamatan']);
    }
}
