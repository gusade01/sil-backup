<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataLingkungan;
use App\Kategori;
use App\File;
use Illuminate\Support\Facades\Storage;
use DB;

class BankDataController extends Controller
{
    public function index(){

        $bulan = array(
            '1' => 'Januari',
            '2' => 'Februari',
            '3' => 'Maret',
            '4' => 'April',
            '5' => 'Mei',
            '6' => 'Juni',
            '7' => 'Juli',
            '8' => 'Agustus',
            '9' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember',
        );

		$kategori = Kategori::all();

        $jumlah = DB::select("SELECT nama_kategori,COUNT(kategori_id) kategori_id FROM tb_detail_dl 
                            JOIN tb_kategori ON tb_detail_dl.kategori_id = tb_kategori.id
                            GROUP BY nama_kategori,kategori_id ORDER BY kategori_id DESC
                            LIMIT 6" );

        $data = DataLingkungan::has('kategori')->orderBy('created_at','DESC')->paginate(6);
        // return response()->json(['data' => $jumlah]);
    	return view('bankdata.index')->with(['list' => $data ,'kategori' => $kategori, 'jumlah' => $jumlah,'bulan'=>$bulan]);

    }

    public function cari(Request $request){

        $jumlah = DB::select("SELECT nama_kategori,COUNT(kategori_id) kategori_id FROM tb_detail_dl 
                            JOIN tb_kategori ON tb_detail_dl.kategori_id = tb_kategori.id
                            GROUP BY nama_kategori,kategori_id ORDER BY kategori_id DESC
                            LIMIT 6" );

    	$kategori = Kategori::all();

        $filters = [
            'kategori' => $request->kategori,
            'tahun' => $request->tahun,
            'bulan' => $request->bulan,
            'nama_dokumen' => $request->doc,
        ];

        $data = DataLingkungan::with('kategori')->whereHas('kategori', function ($query) use ($filters){
            if ($filters['nama_dokumen']) {
                $query->where('nama_dokumen', 'LIKE', '%'.$filters['nama_dokumen'].'%');
            }
            if ($filters['kategori']) {
                $query->where('nama_kategori', '=', $filters['kategori']);
            }
            if ($filters['tahun']){
                $query->whereYear('tahun', '=', $filters['tahun']);
            }
            if ($filters['bulan']){
                $query->where('bulan', '=', $filters['bulan']);
            }

        })->paginate(6);

        return view('bankdata.index',['list' => $data , 'kategori' => $kategori, 'jumlah' => $jumlah]);

    }

    public function cariAll(Request $request){
        $filters = $request->umum;

        $jumlah = DB::select("SELECT nama_kategori,COUNT(kategori_id) kategori_id FROM tb_detail_dl 
                            JOIN tb_kategori ON tb_detail_dl.kategori_id = tb_kategori.id
                            GROUP BY nama_kategori,kategori_id ORDER BY kategori_id DESC
                            LIMIT 6" );

        $kategori = Kategori::all();

        $data = DataLingkungan::with('kategori')->whereHas('kategori', function ($query) use ($filters){
            $query->where('bulan', 'LIKE', '%'.$filters.'%')
            ->whereYear('tahun', 'LIKE', '%'.$filters.'%', 'or')
            ->orWhere('nama_kategori', 'LIKE', '%'.$filters.'%')
            ->orWhere('nama_dokumen', 'LIKE', '%'.$filters.'%')
            ->orWhere('ket_dokumen', 'LIKE', '%'.$filters.'%');
        })->paginate(6);

        // return $data->get();
        return view('bankdata.index',['list' => $data , 'kategori' => $kategori, 'jumlah' => $jumlah]);
    }

    public function download($id){
        $dl = DataLingkungan::with('kategori')->find($id);
        if (!empty($dl->total_dl)) {
            $dl->total_dl = $dl->total_dl + 1;
            $dl->save();
        }else{
            $dl->total_dl = 1;
            $dl->save();
        }
        return Storage::disk('admin')->download($dl->dokumen);
    }
}
