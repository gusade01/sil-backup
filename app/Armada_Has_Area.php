<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Armada_Has_Area extends Model
{
    protected $table = 'm_armada_has_area';

    protected $fillable = [
        'armada_id','area_id'
    ];

    public $timestamps = false;

    public function armada()
    {
        return $this->belongsTo('App\Armada', 'armada_id');
    }
}
