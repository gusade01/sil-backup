<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengangkutan extends Model
{
    protected $table = "t_pengangkutan";

    protected $fillable = [
        "armada_id" , "tanggal", "keterangan" , "bukti_foto" , "tpa_id" , "done"
    ];

    public function armada()
    {
        return $this->belongsTo('App\Armada', 'armada_id');
    }

    public function tpa()
    {
        return $this->belongsTo('App\Tpa', 'tpa_id');
    }

    public function pengangkutan_has_areas()
    {
        return $this->hasMany('App\Pengangkutan_Has_Area', 't_pengangkutan_id');
    }

    public function areas()
    {
        return $this->belongsToMany('App\Area', 't_pengangkutan_has_area', 't_pengangkutan_id', 'area_id');
    }
}
