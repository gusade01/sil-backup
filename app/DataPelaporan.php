<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataPelaporan extends Model
{
    //

    // protected $fillable = [
    //     'tahun', 'bulan', 'dokumen','ket_dokumen',
    // ];

    protected $table = 'tb_data_pelaporan';

    // public function kategori()
    // {
    //     return $this->belongsToMany(Kategori::class, 'tb_detail_dl');
    // }

    public function jenisUsaha()
    {
        $pivotTable = config('admin.database.detail_report');

        $relatedModel = config('admin.database.usaha_model');

        return $this->belongsToMany($relatedModel, $pivotTable, 'id_laporan', 'id_jenis_usaha');
    }

    public function jenisLaporan()
    {
        $pivotTable = config('admin.database.detail_report');

        $relatedModel = config('admin.database.laporan_model');

        return $this->belongsToMany($relatedModel, $pivotTable, 'id_laporan', 'id_jenis_laporan');
    }


    public $timestamps = true;

    // protected $casts = ['kategori_id' => 'array'];
}

