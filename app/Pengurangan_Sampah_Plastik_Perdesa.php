<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengurangan_Sampah_Plastik_Perdesa extends Model
{
    protected $table = 't_pengurangan_sampah_plastik_perdesa';

    protected $fillable = [
        'kelurahan_id' ,'volume','tanggal','keterangan'
    ];

    public function kelurahan()
    {
        return $this->belongsTo('App\Kelurahan', 'kelurahan_id');
    }
    
}
