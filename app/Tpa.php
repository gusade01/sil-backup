<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tpa extends Model
{
    protected $table = 'm_tpa';

    protected $fillable = [
        'nama_tpa' , 'kode' , 'kecamatan_id' , 'kelurahan_id' , 'keterangan' , 'lat' , 'ing'
    ];

    public function pengangkutans()
    {
        return $this->hasMany('App\Pengangkutan', 'tpa_id');
    }

    public function kecamatan()
    {
        return $this->belongsTo('App\Kecamatan', 'kecamatan_id');
    }
    public function kelurahan()
    {
        return $this->belongsTo('App\Kelurahan', 'kelurahan_id');
    }
}
