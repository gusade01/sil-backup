<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisUsaha extends Model
{
    //
    protected $table = 'tb_jenis_usaha';

    // public function lingkungan()
    // {
    //     return $this->belongsToMany(DataLingkungan::class,'tb_detail_dl');
    // }

    public function report()
    {
        $pivotTable = config('admin.database.detail_report');

        $relatedModel = config('admin.database.report_model');

        return $this->belongsToMany($relatedModel, $pivotTable, 'id_jenis_usaha', 'id_laporan');
    }


    protected $casts = ['id_jenis_usaha' => 'array'];
}
