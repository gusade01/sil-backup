<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengangkutan_Has_Area extends Model
{
    protected $table = "t_pengangkutan_has_area";

    protected $fillable = [
        "t_pengangkutan_id" , "area_id" , "is_default" , "volume" , "jam"
    ];

    public function tpa()
    {
        return $this->belongsTo('App\Tpa', 'tpa_id');
    }

    public function pengangkutan()
    {
        return $this->belongsTo('App\Pengangkutan','pengangkutan_id');
    }

    public function area()
    {
        return $this->belongsTo('App\Area', 'area_id');
    }
}
