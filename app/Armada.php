<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Armada extends Model
{
    protected $table = "m_armada";
   
    protected $fillable = [
        'nama_armada' , 'kode' , 'plat_no' , 'user_id'
    ];

    public function pengangkutans()
    {
        return $this->hasMany('App\Pengangkutan', 'armada_id');
    }

    public function areas()
    {
        return $this->belongsToMany('App\Area', 'm_armada_has_area', 'armada_id', 'area_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function armada_Has_Areas()
    {
        return $this->hasMany('App\Armada_Has_Area', 'armada_id');
    }

    
}
