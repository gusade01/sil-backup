<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kabupaten extends Model
{
    protected $table = 'm_kabupaten_new';
    protected $fillable = ['provinsi_id', 'kabupaten'];

    public function kecamatan()
    {
        return $this->hasMany('App\Kecamatan', 'kabupaten_id');
    }
}
