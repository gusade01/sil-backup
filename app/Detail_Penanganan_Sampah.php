<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail_Penanganan_Sampah extends Model
{
    protected $table = 't_detail_penanganan_sampah';

    protected $fillable = [
        'volume' , 'jenis_sampah_id'
    ];

    public function penanganan_Sampah()
    {
        return $this->belongsTo('App\Penanganan_Sampah', 'penanganan_sampah_id');
    }

    public function jenis_sampah()
    {
        return $this->belongsTo('App\Jenis_Sampah', 'jenis_sampah_id');
    }
}
