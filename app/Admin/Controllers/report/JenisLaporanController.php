<?php

namespace App\Admin\Controllers\report;

use App\JenisLaporan;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class JenisLaporanController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Master Jenis Laporan';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new JenisLaporan());

        $grid->disableExport();
        $grid->disableColumnSelector();
        $grid->rows(function (Grid\Row $row){
            $row->column('No', $row->number+1);
        });

        $grid->column('No');
        $grid->column('nama_jenis', __('Nama jenis'));
        $grid->column('ket_laporan', __('Ket laporan'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(JenisLaporan::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('nama_jenis', __('Nama jenis'));
        $show->field('ket_laporan', __('Ket laporan'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new JenisLaporan());

        $form->text('nama_jenis', __('Nama jenis'));
        $form->textarea('ket_laporan', __('Ket laporan'));

        return $form;
    }
}
