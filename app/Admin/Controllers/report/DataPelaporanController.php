<?php

namespace App\Admin\Controllers\report;

use App\DataPelaporan;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class DataPelaporanController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Data Pelaporan';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new DataPelaporan());

        $grid->disableExport();
        $grid->disableColumnSelector();
        $grid->rows(function (Grid\Row $row){
            $row->column('No', $row->number+1);
        });


        $grid->column('No');

        $grid->column('nama_perusahaan', __('Nama Perusahaan'));
        $grid->jenisUsaha()->pluck('nama_jenis')->label();
        $grid->jenisLaporan()->pluck('nama_jenis')->label();
        $grid->column('no_ukl_upl', __('No UKL/UPL'));
        $grid->column('nama_pj', __('Penanggung Jawab'));
        $grid->column('tahun_lapor', __('Tahun lapor'))->date('Y');
        

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(DataPelaporan::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('nama_perusahaan', __('Nama perusahaan'));
        $show->jenisUsaha()->as(function ($roles) {
            return $roles->pluck('nama_jenis');
        })->label();
        $show->jenisLaporan()->as(function ($roles) {
            return $roles->pluck('nama_jenis');
        })->label();
        $show->field('no_ukl_upl', __('No UKL/UPL'));
        $show->field('nama_pj', __('Penanggung Jawab'));
        $show->field('tahun_lapor', __('Tahun lapor'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new DataPelaporan());
        $usahaModel = config('admin.database.usaha_model');
        $laporanModel = config('admin.database.laporan_model');

        $form->footer(function ($footer){
            $footer->disableViewCheck();
            $footer->disableEditingCheck();
            $footer->disableCreatingCheck();
        });

        $form->multipleSelect('jenisUsaha', trans('Jenis Usaha'))
            ->rules('required')
            ->options($usahaModel::all()->pluck('nama_jenis', 'id'));

        $form->multipleSelect('jenisLaporan', trans('Jenis Laporan'))
            ->rules('required')
            ->options($laporanModel::all()->pluck('nama_jenis', 'id'));

        $form->text('nama_perusahaan', __('Nama perusahaan'))->rules('required');
        $form->text('no_ukl_upl', __('No UKL/UPL'))->rules('required');
        $form->text('nama_pj', __('Penanggung Jawab'))->rules('required');

        $thn_start = date('1962');
        $thn_now = date('Y');
        $jarak = range($thn_now,$thn_start);
        $tahun = array_combine($jarak, $jarak);

        $form->select('tahun_lapor', __('Pilih Tahun'))->options($tahun)
            ->rules('required');
        // $form->text('jenis_laporan', __('Jenis laporan'));

        return $form;
    }
}
