<?php

namespace App\Admin\Controllers\report;

use App\JenisUsaha;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class JenisUsahaController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Master Jenis Usaha';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new JenisUsaha());

        $grid->disableExport();
        $grid->disableColumnSelector();
        $grid->rows(function (Grid\Row $row){
            $row->column('No', $row->number+1);
        });


        $grid->column('No');


        // $grid->column('id', __('Id'));
        $grid->column('nama_jenis', __('Jenis Usaha'));
        $grid->column('ket_jenis', __('Keterangan'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(JenisUsaha::findOrFail($id));

        // $show->field('id', __('Id'));
        $show->field('nama_jenis', __('Jenis Usaha'));
        $show->field('ket_jenis', __('Keterangan'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new JenisUsaha());

        $form->text('nama_jenis', __('Nama jenis'));
        $form->textarea('ket_jenis', __('Ket jenis'));

        return $form;
    }
}
