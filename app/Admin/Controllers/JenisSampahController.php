<?php

namespace App\Admin\Controllers;

use App\Jenis_Sampah;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class JenisSampahController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\Jenis_Sampah';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Jenis_Sampah());

        $grid->rows(function (Grid\Row $row) {
            $row->column('number', $row->number+1);
         });
         
         // Your custom column.
        $grid->column('number');
        $grid->column('jenis_sampah', __('Jenis sampah'));
        $grid->column('keterangan', __('Keterangan'));
        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));
        $grid->quickSearch('jenis_sampah');
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Jenis_Sampah::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('jenis_sampah', __('Jenis sampah'));
        $show->field('keterangan', __('Keterangan'));

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->like('jenis_sampah', 'Jenis Sampah');
           
        });
        // $show->field('created_at', __('Created at'));
        // $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Jenis_Sampah());

        $form->text('jenis_sampah', __('Jenis sampah'));
        $form->textarea('keterangan', __('Keterangan'));

        return $form;
    }
}
