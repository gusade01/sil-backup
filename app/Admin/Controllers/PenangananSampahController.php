<?php

namespace App\Admin\Controllers;

use App\Jenis_Sampah;
use App\Penanganan_Sampah;
use App\Tpa;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class PenangananSampahController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\Penanganan_Sampah';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Penanganan_Sampah());

        $grid->column('id', __('Id'));
        $grid->column('tpa_id', __('Tpa id'))->display(function($id){
            return Tpa::find($id)->nama_tpa;
        });
        $grid->column('tanggal', __('Tanggal'));
        $grid->column('keterangan', __('Keterangan'));
        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));
        $grid->column('sampah_terjual', __('Sampah terjual (Dalam Ton)'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Penanganan_Sampah::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('tpa_id', __('Tpa id'))->as(function($id){
            return Tpa::find($id)->nama_tpa;
        });
        $show->field('tanggal', __('Tanggal'));
      
        // $show->field('created_at', __('Created at'));
        // $show->field('updated_at', __('Updated at'));
        $show->field('sampah_terjual', __('Sampah terjual (Dalam Ton)'));
        $show->field('keterangan', __('Keterangan'));

        $show->detail_Penanganan_Sampahs('Detail_Penanganan_Sampah', function($detail){
            $detail->resource('/App/Detail_Penanganan_Sampah');
            
            $detail->rows(function (Grid\Row $row) {
                $row->column('number', $row->number+1);
             });
             
             // Your custom column.
            $detail->column('number');

            $detail->jenis_sampah()->jenis_sampah();
            $detail->volume();
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Penanganan_Sampah());

        $form->select('tpa_id',__('TPA'))->options(Tpa::all()->pluck('nama_tpa', 'id'));
        $form->date('tanggal', __('Tanggal'))->default(date('Y-m-d'));
        $form->decimal('sampah_terjual', __('Sampah terjual'));
        $form->textarea('keterangan', __('Keterangan'));

        $form->hasMany('detail_Penanganan_Sampahs', function (Form\NestedForm $form) {
            $form->select('jenis_sampah_id',__('Jenis Sampah'))->options(Jenis_Sampah::all()->pluck('jenis_sampah', 'id'));
            $form->decimal('volume');
        });


        return $form;
    }
}
