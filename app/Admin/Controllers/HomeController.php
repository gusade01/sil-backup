<?php

namespace App\Admin\Controllers;

use App\Armada;
use App\Http\Controllers\Controller;
use App\Pengangkutan;
use App\Pengangkutan_Has_Area;
use Carbon\Carbon;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Box;
use Encore\Admin\Widgets\InfoBox;
use Encore\Admin\Widgets\Tab;
use Encore\Admin\Widgets\Table;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index(Content $content)
    {
        
        return $content
            ->title('Dashboard')
            ->row(function (Row $row) {
                $row->column(4, function (Column $column) {
                    $infoBox = new InfoBox('Total Armada', 'users', 'aqua', 'admin/kolektor/armadas', Armada::all()->count());
                    $column->append($infoBox->render());
                });
                $row->column(4, function (Column $column) {
                    $date = Carbon::now();
                    $infoBox = new InfoBox('Total Pengangkutan Hari Ini', 'truck', 'aqua', 'admin/kolektor/pengangkutans', Pengangkutan::whereDate('tanggal',$date)->get()->count());

                    $column->append($infoBox->render());
                });
                $row->column(4, function (Column $column) {
                    $date = Carbon::now();
                    $pengangkutans = Pengangkutan::whereDate('tanggal',$date)->get();
                    $volume = 0;
                    foreach($pengangkutans as $pengangkutan){
                        $volume += $pengangkutan->pengangkutan_Has_Areas->sum('volume');
                    }
                    $infoBox = new InfoBox('total Volume Sampah Hari Ini', 'recycle', 'aqua', 'admin/kolektor/pengangkutans', $volume);

                    $column->append($infoBox->render());
                });
               
            })
            ->row(function(Row $row){
                $row->column(8, function (Column $column) {
                    $label = [];
                    $tanggal_untuk_perhitungan = [];
                    $data = [];
                    for($loop = 0 ; $loop < 7; $loop++){
                        $label[$loop] = Carbon::now()->subDay(7-$loop)->format('d F Y');
                        $tanggal_untuk_perhitungan[$loop] = Carbon::now()->subDay(7-$loop);
                    }
                    $loop = 0;
                    foreach($tanggal_untuk_perhitungan as $tanggal_volume){
                        $volume = 0;
                        $pengangkutans = Pengangkutan::whereDate('tanggal',$tanggal_volume)->get();
                        foreach($pengangkutans as $pengangkutan){
                            $volume += $pengangkutan->pengangkutan_Has_Areas->sum('volume'); 
                        }
                        $data[$loop] = $volume;
                        $loop++;
                    }

                    $column->append(new Box('grafik volume sampah', view('admin.LineChart',['label'=>$label , 'data'=>$data , 'title'=>'Grafik Total Volume Sampah'])));
                });

                $row->column(4, function(Column $column){
                    $headers = ['Number', 'Kelurahan', 'Total Pengangkutan', 'Total Volume'];
                    $datas = DB::select(DB::raw("SELECT m_kelurahan_new.`kelurahan` , COUNT(t_pengangkutan.id) AS total_pengangkutan, SUM(t_pengangkutan_has_area.`volume`) AS total_volume FROM t_pengangkutan
                    INNER JOIN t_pengangkutan_has_area ON  t_pengangkutan_has_area.`t_pengangkutan_id` = t_pengangkutan.`id`
                    INNER JOIN m_area ON m_area.`id` = t_pengangkutan_has_area.`area_id`
                    INNER JOIN m_kelurahan_new ON m_kelurahan_new.`id` = m_area.`kelurahan_id`
                    WHERE CURDATE() = DATE(t_pengangkutan.`tanggal`)
                    GROUP BY kelurahan
                    ORDER BY total_volume DESC"));
                    // $rows = [
                    //     [1, 'labore21@yahoo.com', 'Ms. Clotilde Gibson', 'Goodwin-Watsica'],
                    //     [2, 'omnis.in@hotmail.com', 'Allie Kuhic', 'Murphy, Koepp and Morar'],
                    //     [3, 'quia65@hotmail.com', 'Prof. Drew Heller', 'Kihn LLC'],
                    //     [4, 'xet@yahoo.com', 'William Koss', 'Becker-Raynor'],
                    //     [5, 'ipsa.aut@gmail.com', 'Ms. Antonietta Kozey Jr.'],
                    //     [1, 'labore21@yahoo.com', 'Ms. Clotilde Gibson', 'Goodwin-Watsica'],
                    //     [2, 'omnis.in@hotmail.com', 'Allie Kuhic', 'Murphy, Koepp and Morar'],
                    //     [3, 'quia65@hotmail.com', 'Prof. Drew Heller', 'Kihn LLC'],
                    //     [4, 'xet@yahoo.com', 'William Koss', 'Becker-Raynor'],
                    // ];

                    $rows = [];
                    $index = 1;
                    foreach($datas as $data){
                        if($index == 10){
                            break;
                        }
                        $rows[$index-1] = [$index,$data->kelurahan,$data->total_pengangkutan,$data->total_volume];
                        $index++;
                    }

                    $table = new Table($headers, $rows);

                    $column->append(new Box("Kelurahan dengan Volume Sampah Terbesar",$table->render()));
                });
            });
    }
}
