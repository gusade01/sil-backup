<?php

namespace App\Admin\Controllers;

use App\Area;
use App\Kecamatan;
use App\Kelurahan;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class AreaController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\Area';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Area());

        $grid->rows(function (Grid\Row $row) {
            $row->column('number', $row->number+1);
         });
         
         // Your custom column.
        $grid->column('number');

        // $grid->column('id', __('Id'));
        $grid->column('nama_area', __('Nama area'));
        $grid->column('kode', __('Kode'));
        $grid->column('kecamatan_id', __('Kecamatan'))->display(function($id){
            return Kecamatan::find($id)->kecamatan;
        });
        $grid->column('kelurahan_id', __('Kelurahan'))->display(function($id){
            return Kelurahan::find($id)->kelurahan;
        });
        $grid->column('keterangan', __('Keterangan'));
        $grid->column('lat', __('Lat'));
        $grid->column('lng', __('Lng'));
        
    
        $grid->quickSearch('nama_area', 'kode', 'id');
        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->like('nama_area', 'Nama Area');
            $filter->like('kode', 'Kode');
           
            // Add a column filter
        });

 
        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Area::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('nama_area', __('Nama area'));
        $show->field('kode', __('Kode'));
        $show->field('kecamatan_id', __('Kecamatan'))->as(function($id){
            return Kecamatan::find($id)->kecamatan;
        });
        $show->field('kelurahan_id', __('Kecamatan'))->as(function($id){
            return Kelurahan::find($id)->kelurahan;
        });
        $show->field('keterangan', __('Keterangan'));
        $show->field('Position')->latlong('lat', 'lng', $height = 400, $zoom = 16);

        // $show->field('created_at', __('Created at'));
        // $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Area());

        $form->text('nama_area', __('Nama area'));
        $form->text('kode', __('Kode'));
        // $form->select('kecamatan_id')->options(function ($id) {
        //     $kecamatan = Kecamatan::find($id);
        
        //     if ($kecamatan) {
        //         return [$kecamatan->id => $kecamatan->kecamatan];
        //     }
        // });

        $form->select('kecamatan_id',__('Kecamatan'))->options(Kecamatan::where('kabupaten_id',5103)->get()->pluck('kecamatan', 'id'));
        $form->select('kelurahan_id',__('Kelurahan'))->options(Kelurahan::all()->pluck('kelurahan', 'id'));
        // $form->number('kecamatan_id', __('Kecamatan id'));
        // $form->number('kelurahan_id', __('Kelurahan id'));
        $form->textarea('keterangan', __('Keterangan'));
        // $form->decimal('lat', __('Lat'));
        // $form->decimal('lng', __('Lng'));
        $form->latlong('lat', 'lng', 'Position')->height(500);
        return $form;
    }
}
