<?php

namespace App\Admin\Controllers;

use App\Kelurahan;
use App\Kecamatan;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class KelurahanController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\Kelurahan';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Kelurahan());

        $grid->disableCreateButton();
        $grid->disableActions();

        $grid->rows(function (Grid\Row $row) {
            $row->column('number', $row->number+1);
         });
         
         // Your custom column.
        $grid->column('number');
        $grid->column('kecamatan_id', __('Kecamatan'))->display(function($id) {
            return Kecamatan::find($id)->kecamatan;
        })->filter();
        $grid->column('kelurahan', __('Kelurahan'));
        $grid->quickSearch('kelurahan', 'id');
        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->like('kelurahan', 'Kelurahan');
           
            // Add a column filter
        });
        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Kelurahan::findOrFail($id));

        $show->field('id', __('Id'));
        // $show->field('created_at', __('Created at'));
        // $show->field('updated_at', __('Updated at'));
        $show->field('kecamatan_id', __('Kecamatan'))->as(function($id){
            return Kecamatan::find($id)->kecamatan;
        });
        $show->field('kelurahan', __('Kelurahan'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Kelurahan());

        $form->select('kecamatan_id',__('Kecamatan'))->options(Kecamatan::all()->pluck('kecamatan', 'id'));
        $form->text('kelurahan', __('Kelurahan'));

        return $form;
    }
}
