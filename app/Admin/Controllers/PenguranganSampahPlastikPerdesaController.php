<?php

namespace App\Admin\Controllers;

use App\Kelurahan;
use App\Pengurangan_Sampah_Plastik_Perdesa;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class PenguranganSampahPlastikPerdesaController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\Pengurangan_Sampah_Plastik_Perdesa';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Pengurangan_Sampah_Plastik_Perdesa());

        $grid->column('id', __('Id'));
        $grid->column('kelurahan_id', __('Kelurahan id'))->display(function($id){
            return Kelurahan::find($id)->kelurahan;
        });
        $grid->column('volume', __('Volume'));
        $grid->column('tanggal', __('Tanggal'));
        $grid->column('keterangan', __('Keterangan'));
        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Pengurangan_Sampah_Plastik_Perdesa::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('kelurahan_id', __('Kelurahan id'))->as(function($id){
            return Kelurahan::find($id)->kelurahan;
        });
        $show->field('volume', __('Volume'));
        $show->field('tanggal', __('Tanggal'));
        $show->field('keterangan', __('Keterangan'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Pengurangan_Sampah_Plastik_Perdesa());

        $form->select('kelurahan_id',__('Kelurahan'))->options(Kelurahan::all()->pluck('kelurahan', 'id'));
        $form->decimal('volume', __('Volume'));
        $form->date('tanggal', __('Tanggal'))->default(date('Y-m-d'));
        $form->textarea('keterangan', __('Keterangan'));

        return $form;
    }
}
