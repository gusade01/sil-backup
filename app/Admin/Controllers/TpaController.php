<?php

namespace App\Admin\Controllers;

use App\Kecamatan;
use App\Kelurahan;
use App\Tpa;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class TpaController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\Tpa';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Tpa());

        $grid->rows(function (Grid\Row $row) {
            $row->column('number', $row->number+1);
         });
         
         // Your custom column.
        $grid->column('number');
        $grid->column('nama_tpa', __('Nama tpa'));
        $grid->column('kode', __('Kode'));
        $grid->column('kecamatan_id', __('Kecamatan'))->display(function($id){
            return Kecamatan::find($id)->kecamatan;
        });
        $grid->column('kelurahan_id', __('Kelurahan'))->display(function($id){
            return Kelurahan::find($id)->kelurahan;
        });
        $grid->column('keterangan', __('Keterangan'));
        $grid->column('lat', __('Lat'));
        $grid->column('ing', __('Ing'));
        $grid->quickSearch('nama_tpa', 'kode', 'id');
        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->like('nama_tpa', 'Nama TPA');
            $filter->like('kode', 'Kode');
            // Add a column filter
        });
        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Tpa::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('nama_tpa', __('Nama tpa'));
        $show->field('kode', __('Kode'));
        $show->field('kecamatan_id', __('Kecamatan id'));
        $show->field('kelurahan_id', __('Kelurahan id'));
        $show->field('keterangan', __('Keterangan'));
        $show->field('Position')->latlong('lat', 'lng', $height = 400, $zoom = 16);
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Tpa());

        $form->text('nama_tpa', __('Nama tpa'));
        $form->text('kode', __('Kode'));
        $form->select('kecamatan_id',__('Kecamatan'))->options(Kecamatan::all()->pluck('kecamatan', 'id'));
        $form->select('kelurahan_id',__('Kelurahan'))->options(Kelurahan::all()->pluck('kelurahan', 'id'));
        $form->textarea('keterangan', __('Keterangan'));
        $form->latlong('lat', 'lng', 'Position')->height(500);
        return $form;
    }
}
