<?php

namespace App\Admin\Controllers;

use App\Armada;
use App\Pengangkutan;
use App\Tpa;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class PengangkutanController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\Pengangkutan';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Pengangkutan());

        $grid->disableCreateButton();
    

        $grid->rows(function (Grid\Row $row) {
            $row->column('number', $row->number+1);
         });
         
         // Your custom column.
        $grid->column('number');
        $grid->column('armada_id', __('Supir'))->display(function($id){
            return Armada::find($id)->nama_armada;
        });
        // $grid->column('armada_id', __('Nama armada'))->display(function($id){
        //     return Armada::find($id)->nama_armada;
        // });
        $grid->column('tanggal', __('Tanggal'))->date('d-m-Y H:i:s');
        $grid->column('keterangan', __('Keterangan'));
       
        $grid->column('bukti_foto', __('Bukti foto'))->display(function($bukti_foto){
            
            return "http://127.0.0.1:8000/storage/".$bukti_foto;
        })->link();
        $grid->column('tpa_id', __('Tpa'))->display(function($id){
            return Tpa::find($id)->nama_tpa;
        });
        $grid->column('done', __('Done'))->using([1 => 'selesai', 0 => 'belum selesai'])->filter();
        $grid->quickSearch('tanggal', 'done', 'id');
        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->like('nama_area', 'Nama Area');
            $filter->like('kode', 'Kode');
            $filter->between('Rentang Tanggal')->dateTime();
            $filter->date('tanggal');


            // Add a column filter
        });
        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Pengangkutan::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('armada_id', __('Nama Armada'))->as(function($id){
            return Armada::find($id)->nama_armada;
        });
        // $show->field('armada_id', __('Sopir'))->as(function($id){
        //     return Armada::find($id)->user->name;
        // });
        $show->field('tanggal', __('Tanggal'))->date('d-m-Y H:i:s');
        $show->field('keterangan', __('Keterangan'));
        // $show->field('created_at', __('Created at'));
        // $show->field('updated_at', __('Updated at'));
        $show->field('bukti_foto', __('Bukti foto'))->as(function($bukti_foto){
            return "http://127.0.0.1:8000/storage/".$bukti_foto;
        })->image();

        $show->field('tpa_id', __('Tpa id'))->as(function($id){
            return Tpa::find($id)->nama_tpa;
        });
        $show->field('done', __('Done'))->using([1 => 'selesai', 0 => 'belum selesai']);

        $show->pengangkutan_Has_Areas('Pengangkutan_Has_Areas', function($pengangkutan_Has_Area){
            $pengangkutan_Has_Area->resource('/App/Pengangkutan_Has_Area');
            
            $pengangkutan_Has_Area->rows(function (Grid\Row $row) {
                $row->column('number', $row->number+1);
             });
             
             // Your custom column.
            $pengangkutan_Has_Area->column('number');

            $pengangkutan_Has_Area->area()->nama_area('Nama Area');
            $pengangkutan_Has_Area->is_default('Area Default')->using([1 => 'Area Default', 0 => 'Area tidak Default']);
            $pengangkutan_Has_Area->volume();
            $pengangkutan_Has_Area->jam();
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Pengangkutan());

        $form->number('armada_id', __('Armada id'));
        $form->datetime('tanggal', __('Tanggal'))->default(date('Y-m-d H:i:s'));
        $form->textarea('keterangan', __('Keterangan'));
        $form->text('bukti_foto', __('Bukti foto'));
        $form->number('tpa_id', __('Tpa id'));
        $form->switch('done', __('Done'));

        return $form;
    }
}
