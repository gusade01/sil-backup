<?php

namespace App\Admin\Controllers;

use App\Lingkungan;
use App\Kelurahan;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class LingkunganController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\Lingkungan';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Lingkungan());

        $grid->disableCreateButton();
        $grid->disableActions();

        $grid->rows(function (Grid\Row $row) {
            $row->column('number', $row->number+1);
         });
         
         // Your custom column.
        $grid->column('number');
        $grid->column('kelurahan_id', __('Kelurahan id'))->display(function($id) {
            return Kelurahan::find($id)->kelurahan;
        });
        $grid->column('lingkungan', __('Lingkungan'));
        $grid->quickSearch('lingkungan', 'id');
        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->like('lingkungan', 'Lingkungan');
            // Add a column filter
        });
        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Lingkungan::findOrFail($id));

        $show->field('id', __('Id'));
        // $show->field('created_at', __('Created at'));
        // $show->field('updated_at', __('Updated at'));
        $show->field('kelurahan_id', __('Kelurahan id'))->as(function($id){
            return Kelurahan::find($id)->kelurahan;
        });
        $show->field('lingkungan', __('Lingkungan'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Lingkungan());


        $form->select('kelurahan_id',__('Kelurahan'))->options(Kelurahan::all()->pluck('kelurahan', 'id'));
        $form->text('lingkungan', __('Lingkungan'));

        return $form;
    }
}
