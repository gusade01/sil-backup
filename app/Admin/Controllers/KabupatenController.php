<?php

namespace App\Admin\Controllers;

use App\Kabupaten;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class KabupatenController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\Kabupaten';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Kabupaten());

        $grid->disableCreateButton();
        $grid->disableActions();

        $grid->rows(function (Grid\Row $row) {
            $row->column('number', $row->number+1);
         });
         
         // Your custom column.
        $grid->column('number');
        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));
        // $grid->column('provinsi_id', __('Provinsi id'));
        $grid->column('kabupaten', __('Kabupaten'))->filter();
        $grid->quickSearch('kabupaten', 'id');
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Kabupaten::findOrFail($id));

        $show->field('id', __('Id'));
        // $show->field('created_at', __('Created at'));
        // $show->field('updated_at', __('Updated at'));
        // $show->field('provinsi_id', __('Provinsi id'));
        $show->field('kabupaten', __('Kabupaten'));

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->like('kabupaten', 'Kabupaten');
           
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Kabupaten());
        

        $form->text('kabupaten', __('Kabupaten'));

        return $form;
    }
}
