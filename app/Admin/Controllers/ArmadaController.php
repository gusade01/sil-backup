<?php

namespace App\Admin\Controllers;

use App\Area;
use App\Armada;
use App\User;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ArmadaController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\Armada';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Armada());

        $grid->rows(function (Grid\Row $row) {
            $row->column('number', $row->number+1);
         });
         
         // Your custom column.
        $grid->column('number');
        $grid->column('nama_armada', __('Nama armada'))->filter();
        $grid->column('kode', __('Kode'))->filter();
        $grid->column('plat_no', __('Plat no'))->filter();
        $grid->column('user_id', __('sopir'))->display(function($id){
            return User::find($id)->name;
        })->filter();

        $grid->quickSearch('nama_armada', 'kode', 'plat_no');
        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->like('nama_armada', 'Nama Armda');
            $filter->like('kode', 'Kode');
            $filter->like('plat_no','Plat Nomer Kendaraan');
            // Add a column filter
        });

        $grid->areas()->display(function ($areas) {
            $areas = array_map(function ($area) {
                return "<span class='label label-success'>{$area['nama_area']}</span><br>";
            }, $areas);
            return join('&nbsp;', $areas);
        });
        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Armada::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('nama_armada', __('Nama armada'));
        $show->field('kode', __('Kode'));
        $show->field('plat_no', __('Plat no'));
        $show->field('user_id', __('Sopir'))->as(function($id){
            return User::find($id)->name;
        });
        $show->areas('Areas', function($areas){
            $grid = new Grid(new Armada());

            $grid->rows(function (Grid\Row $row) {
                $row->column('number', $row->number+1);
             });

             $areas->nama_area();
             $areas->kode();
             
        });
        // $show->field('created_at', __('Created at'));
        // $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Armada());

        $form->text('nama_armada', __('Nama armada'));
        $form->text('kode', __('Kode'));
        $form->text('plat_no', __('Plat no'));
        $form->select('user_id',__('Sopir'))->options(User::all()->pluck('name', 'id'));

        $form->hasMany('armada_Has_Areas',__('Area'), function (Form\NestedForm $form) {
            $form->select('area_id',__('Area'))->options(Area::all()->pluck('nama_area', 'id'));
        });

        return $form;
    }
}
