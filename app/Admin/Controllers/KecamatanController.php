<?php

namespace App\Admin\Controllers;

use App\Kecamatan;
use App\Kabupaten;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class KecamatanController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\Kecamatan';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Kecamatan());

        $grid->disableCreateButton();
        $grid->disableActions();

        $grid->rows(function (Grid\Row $row) {
            $row->column('number', $row->number+1);
         });
         
         // Your custom column.
        $grid->column('number');
        $grid->column('kabupaten_id', __('Kabupaten id'))->display(function($id) {
            return Kabupaten::find($id)->kabupaten;
        });
        $grid->column('kecamatan', __('Kecamatan'))->filter();
        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));
        $grid->quickSearch('kecamatan', 'id');
        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->like('kecamatan', 'Kecamatan');
            
            // Add a column filter
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Kecamatan::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('kabupaten_id', __('Kabupaten id'))->as(function($id){
            return Kabupaten::find($id)->kabupaten;
        });
        $show->field('kecamatan', __('Kecamatan'));


        // $show->field('created_at', __('Created at'));
        // $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Kecamatan());

        $form->select('kabupaten_id',__('Kabupaten'))->options(Kabupaten::all()->pluck('kabupaten', 'id'));
        $form->text('kecamatan', __('Kecamatan'));

        return $form;
    }
}
