<?php

namespace App\Admin\Controllers\bankData;

use App\Kategori;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class KategoriController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Master Kategori';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Kategori());

        $grid->disableExport();
        $grid->disableColumnSelector();
        
        $grid->rows(function (Grid\Row $row){
            $row->column('No', $row->number+1);
        });


        $grid->column('No');


        // $grid->column('id', __('Id'));
        $grid->column('nama_kategori', __('Nama kategori'));
        $grid->column('keterangan', __('Keterangan'));
        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();

            $filter->where(function ($query) {
                $query->where('nama_kategori', 'like', "%{$this->input}%")
                ->orWhere('keterangan','like',"%{$this->input}%");
            }, 'Pencarian')->placeholder('Apa yang ingin anda cari ?');
            
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Kategori::findOrFail($id));

        // $show->field('id', __('Id'));
        $show->field('nama_kategori', __('Nama kategori'));
        $show->field('keterangan', __('Keterangan'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Kategori());

        $kategoriModel = config('admin.database.kategori_model');

        $form->footer(function ($footer){
            $footer->disableViewCheck();
            $footer->disableEditingCheck();
            $footer->disableCreatingCheck();
        });

        $form->text('nama_kategori', __('Nama kategori'))
        ->rules('required');

        $form->textarea('keterangan', __('Keterangan'))
        ->rules('required');

        return $form;
    }
}
