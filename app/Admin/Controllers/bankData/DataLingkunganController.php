<?php

namespace App\Admin\Controllers\bankData;

use App\DataLingkungan;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class DataLingkunganController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Bank Data Lingkungan';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {

        $bulan = array(
            '1' => 'Januari',
            '2' => 'Februari',
            '3' => 'Maret',
            '4' => 'April',
            '5' => 'Mei',
            '6' => 'Juni',
            '7' => 'Juli',
            '8' => 'Agustus',
            '9' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember',
        );

        $grid = new Grid(new DataLingkungan());

        $grid->disableExport();
        $grid->disableColumnSelector();
        // $grid->column('id', __('Id'));
        // $grid->id();
        $grid->rows(function (Grid\Row $row){
            $row->column('No', $row->number+1);
        });

        $grid->quickSearch('nama_dokumen', 'dokumen', 'id');
        $grid->column('No');


        $grid->kategori()->pluck('nama_kategori')->label()->filter();
        $grid->tahun();

        $grid->column('bulan')->display(function () use ($bulan) {
            return $bulan[$this->bulan];
        });

        $grid->column('nama_dokumen', __('Nama Dokumen'));
        $grid->dokumen()->downloadable();
        $grid->column('ket_dokumen', __('Keterangan'));
        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();

            $filter->where(function ($query) {
                $query->whereHas('kategori', function ($query) {
                    $query->where('nama_kategori', 'like', "%{$this->input}%")
                    ->orWhere('tahun','like',"%{$this->input}%")
                    ->orWhere('nama_dokumen','like',"%{$this->input}%")
                    ->orWhere('dokumen','like',"%{$this->input}%")
                    ->orWhere('ket_dokumen','like',"%{$this->input}%")
                    ->orWhere('bulan','like',"%{$this->input}%");
                });

            }, 'Pencarian')->placeholder('Apa yang ingin anda cari ?');
            
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {

        $bulan = array(
            '1' => 'Januari',
            '2' => 'Februari',
            '3' => 'Maret',
            '4' => 'April',
            '5' => 'Mei',
            '6' => 'Juni',
            '7' => 'Juli',
            '8' => 'Agustus',
            '9' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember',
        );

        $show = new Show(DataLingkungan::findOrFail($id));
        // $show->field('id', __('Id'));
        // $show->id();
        $show->kategori()->as(function ($roles) {
            return $roles->pluck('nama_kategori');
        })->label();
        $show->tahun();

        // $show->bulan();
        $show->bulan()->as(function () use ($bulan) {
            return $bulan[$this->bulan];
        });

        $show->dokumen()->file();
        $show->ket_dokumen();
        // $show->field('created_at', __('Created at'));
        // $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new DataLingkungan());
        $kategoriModel = config('admin.database.kategori_model');

        $bulan = array(
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember',
        );

        $form->footer(function ($footer){
            $footer->disableViewCheck();
            $footer->disableEditingCheck();
            $footer->disableCreatingCheck();
        });

        $form->multipleSelect('kategori', trans('Pilih Kategori'))
            ->rules('required')
            ->options($kategoriModel::all()->pluck('nama_kategori', 'id'));

        $thn_start = date('1962');
        $thn_now = date('Y');
        $jarak = range($thn_now,$thn_start);
        $tahun = array_combine($jarak, $jarak);

        $form->select('tahun', __('Pilih Tahun'))->options($tahun)
            ->rules('required');

        $form->select('bulan', __('Pilih Bulan'))->options($bulan)
            ->rules('required');

        $cek = DataLingkungan::all('id');
        if ($cek->isEmpty()){
            $idAkhir = 1;
            $tambahAkhir = substr($idAkhir, -3); 
            $idBaru = str_pad($tambahAkhir, 3, 0, STR_PAD_LEFT);
            // return 'DL-'.date("Ym").'-'.$idBaru.'.'.$file->guessExtension();
        }else{
            $idAkhir = DataLingkungan::orderBy('id','desc')->first()->id;
            $tambahAkhir = substr($idAkhir, -3); 
            $idBaru = str_pad($tambahAkhir, 3, 0, STR_PAD_LEFT);
            // return 'DL-'.date("Ym").'-'.$idBaru.'.'.$file->guessExtension();
        }

        $form->text('nama_dokumen',__('Nama Dokumen'))
        ->rules('required');

        $form->file('dokumen', __('Pilih Dokumen'))
            ->rules(['required','mimes:doc,docx,xlsx,pptx,pdf,txt,csv,rtf'])
            ->name(function ($file) use ($idBaru){
                return 'BD'.date("Ym").$idBaru.'.'.$file->guessExtension();
            });

        $form->textarea('ket_dokumen', __('Isi Keterangan dokumen'))
            ->rules('required');


        return $form;
    }
}
