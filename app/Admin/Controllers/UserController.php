<?php

namespace App\Admin\Controllers;

use App\User;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class UserController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\User';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User());

        $grid->column('id', __('Id'));
        $grid->column('name', __('Name'));
        $grid->column('email', __('Email'));
        
        $grid->column('avatar', __('Avatar'))->display(function($avatar){
            return "http://127.0.0.1:8000/storage/".$avatar;
        })->link();
        $grid->column('username', __('Username'));

        $grid->quickSearch('name','email','username');
        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->like('nama', 'Nama');
            $filter->like('email', 'Email');
            $filter->like('username','Username');
            // Add a column filter
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('avatar', __('Avatar'))->as(function($avatar){
            return "http://127.0.0.1:8000/storage/".$avatar;
        })->image();
        $show->field('username', __('Username'));
        $show->field('email', __('Email'));
        

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User());

        $form->text('name', __('Name'));
        $form->image('avatar', __('Avatar'));
        $form->text('username', __('Username'));
        $form->email('email', __('Email'));
        $form->password('password', __('Password'));


        return $form;
    }
}
