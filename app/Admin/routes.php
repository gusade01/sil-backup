<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('admin.home');
    $router->resource('kolektor/kecamatan', KecamatanController::class);
    $router->resource('kolektor/kabupaten', KabupatenController::class);
    $router->resource('kolektor/kelurahan', KelurahanController::class);
    $router->resource('kolektor/lingkungan', LingkunganController::class);
    $router->resource('kolektor/tpst', TpstController::class);
    $router->resource('kolektor/areas', AreaController::class);
    $router->resource('kolektor/armadas', ArmadaController::class);
    $router->resource('kolektor/tpas', TpaController::class);
    $router->resource('kolektor/pengangkutans', PengangkutanController::class);
    $router->resource('kolektor/jenis-sampah', JenisSampahController::class);
    $router->resource('kolektor/users', UserController::class);
    $router->resource('kolektor/penanganan-sampah', PenangananSampahController::class);
    $router->resource('kolektor/pengurangan-sampah-plastik', PenguranganSampahPlastikPerdesaController::class);

    //Bank Data Lingkungan
    $router->resource('bank-data/kategori', 'bankData\KategoriController');
    $router->resource('bank-data/data', 'bankData\DataLingkunganController');
    //E-Report UKL/PKL
    $router->resource('report/jenis-usaha', 'report\JenisUsahaController');
    $router->resource('report/data', 'report\DataPelaporanController');
    $router->resource('report/jenis-laporan', 'report\JenisLaporanController');
});
