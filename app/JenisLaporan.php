<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisLaporan extends Model
{
    //
    protected $table = 'tb_jenis_laporan';

    // public function lingkungan()
    // {
    //     return $this->belongsToMany(DataLingkungan::class,'tb_detail_dl');
    // }

    public function report()
    {
        $pivotTable = config('admin.database.detail_report');

        $relatedModel = config('admin.database.laporan_model');

        return $this->belongsToMany($relatedModel, $pivotTable, 'id_jenis_laporan', 'id_laporan');
    }


    protected $casts = ['id_jenis_laporan' => 'array'];
}
