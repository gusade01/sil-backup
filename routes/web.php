<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'HomeController@index')->name('home');
Route::get('/bank-data','BankDataController@index')->name('bank-data');
Route::get('/bank-data/cari','BankDataController@cari');
Route::get('/bank-data/cariAll','BankDataController@cariAll');
Route::get('/bank-data/download/{id}','BankDataController@download')->name('downloadfile');