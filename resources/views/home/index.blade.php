@extends('layouts.layout1')
@section('title')
Landing
@endsection
@section('content')
<div class="slider_area">
    <div class="single_slider  d-flex align-items-center slider_bg_1">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-xl-10">
                    <div class="slider_text text-center justify-content-center">
                    {{--  <h3>Bank Data</h3>  --}}

                    <div class="logo">
                        <a href="index-2.html">
                            <img src="front/img/logo portal3.png" alt="">
                         </a>
                    </div>

                    <section id="services">
                            <div class="container">
                                <!-- Example row of columns -->
                                <div class="row features">
                                    <div class="col-md-12 text-center">
                            
                                    </div>
                                </div>
                                <div class="row features">
                                    <div class="col-lg-2 col-sm-4">
                                        <div class="hexicon hvr-grow">
                                            <span class="hexicontext">Website DLHK</span>
                                            <a href="http://badungkab.go.id/instansi/dislhk/"><div class="inicon website"></div></a>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-4">
                                        <div class="hexicon hvr-grow">
                                            <span class="hexicontext">Armada Pengangkutan</span>
                                            <a href="admin"><div class="inicon armada"></div></a>
                                        </div>
                                    </div>
                            
                                    <div class="col-lg-2 col-sm-4">
                                        <div class="hexicon hvr-grow">
                                            <span class="hexicontext">Bank Data</span>
                                            <a href="bank-data"><div class="inicon bank-data"></div></a>
                                        </div>
                                    </div>
                            
                                    <div class="col-lg-2 col-sm-4">
                                        <div class="hexicon hvr-grow">
                                            <span class="hexicontext">E-Report</span>
                                            <a href="#"><div class="inicon e-report"></div></a>
                                        </div>
                                    </div>
                            
                                    <div class="col-lg-2 col-sm-4">
                                        <div class="hexicon hvr-grow">
                                            <span class="hexicontext">Pengaduan</span>
                                            <a href="https://sidumas.badungkab.go.id/"><div class="inicon pengaduan"></div></a>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-4">
                                        <div class="hexicon hvr-grow">
                                            <span class="hexicontext">Pengurangan Sampah</span>
                                            <a href="#"><div class="inicon pengurangan"></div></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection