<canvas id="myChart" width="400" height="200"></canvas>
<script>
$(function () {
    var ctx = document.getElementById("myChart").getContext('2d');
    var labels = {!! json_encode($label) !!};
    var data =  {!! json_encode($data) !!};
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: {!! json_encode($title) !!},
                data: data,
                backgroundColor: 'rgba(88,188,130,0.5)',
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
});
</script>