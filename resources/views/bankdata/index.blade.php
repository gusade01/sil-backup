@extends('layouts.bdlayout')
@section('title')
Bank Data Lingkungan
@endsection
@section('content')


  <div class="slider_area">
        <div class="single_slider  d-flex align-items-center slider_bg_1">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-xl-8">
                        <div class="slider_text text-center justify-content-center">
                            <h3><strong>BANK DATA LINGKUNGAN</strong></h3>
                            <div class="search_form">
                                <form action="/bank-data/cariAll" method="GET">
                                    <div class="row align-items-center">
                                        <div class="col-sm-8 col-xs-7">
                                            <div class="input_field">
                                                <input type="text" name="umum" placeholder="Apa yang ingin anda Cari ?">
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xs-5">
                                            <div class="button_search">
                                                <button class="boxed-btn2" type="submit">Cari</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="quality">
                                <ul>
                                    @foreach($jumlah as $j)
                                    <li>
                                        <button>{{$j->nama_kategori}}</button>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="site-section bg-light">
        <div class="container">
            <div class="row justify-content-start text-left mb-4">
                <div class="col-md-9" data-aos="fade">
                    <h3 class="font-weight-bold text-black">List Dokumen</h3>
                </div>
                <div class="col-md-3 text-right" data-aos="fade" data-aos-delay="200">
                  <button type="button" id="cariAll" class="btn btn-success btn-md" id="myBtn">Cari Dokumen</button>
                </div>
            </div>
            <div class="explorer_europe list_wrap" id="form">
              <div class="row mb-4">
                <div class="col-xl-12 col-lg-12">
                    <div class="filter_wrap">
                        <div class="filter_main_wrap">
                            <div class="filter_inner">
                                <form action="/bank-data/cari" method="GET">
                                    <div class="input_field">
                                        <input type="text" name="doc" placeholder="Cari Nama Dokumen">
                                    </div>
                                    <div class="input_field">
                                        <select class="selectpicker" name="kategori" title="Pilih Kategori" data-live-search="true">
                                            @foreach ($kategori as $k)
                                            <option>{{$k->nama_kategori}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="input_field">
                                        <select class="selectpicker" name="tahun" title="Pilih Tahun" data-live-search="true">
                                            @for ($year = date('Y'); $year > date('Y') - 100; $year--)
                                            <option value="{{$year}}">{{$year}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="input_field">
                                        <select class="selectpicker" name="bulan" title="Pilih Bulan" data-live-search="true">
                                            @foreach (range (1,12) as $month)
                                            <option value="{{$month}}">{{date("F", strtotime('2016-'.$month))}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="last_range">
                                        <button class="boxed-btn2">Cari Data</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>  
              </div>
            </div>
            @foreach($list as $daftar)
            <div class="row" data-aos="fade">
                <div class="col-md-12">
                    <div class="job-post-item bg-white p-4 d-block d-md-flex align-items-center">
                        <div class="mb-4 mb-md-0 mr-5">
                            <div class="job-post-item-header d-flex align-items-center">
                                <h2 class="mr-3 text-black h5">{{$daftar->nama_dokumen}}</h2>
                                <div class="badge-wrap">
                                    <span class="bg-success text-white badge py-2 px-2">{{$daftar->kategori->pluck('nama_kategori')->implode(', ')}}</span>
                                </div>
                            </div>
                            <div class="job-post-item-body d-block d-md-flex">
                                <div class="mr-3"><span class="fl-bigmug-line-portfolio23"></span>{{$daftar->ket_dokumen}}</div>
                            </div>
                            <div class="job-post-item-body d-block d-md-flex">
                                <?php
                                    $bln = date('F', mktime(0,0,0,$daftar->bulan,10));
                                ?>
                                <div class="mr-3"><span class="fl-bigmug-line-portfolio23"></span>{{$bulan[$daftar->bulan]}},{{$daftar->tahun}}</div>
                            </div>
                            <div class="job-post-item-body d-block d-md-flex">
                                <div class="mr-3"><span class="fl-bigmug-line-portfolio23 text-black">Total Unduhan : {{$daftar->total_dl}}</span></div>
                            </div>                           
                        </div>
                        <div class="ml-auto">
{{--                             <a href="{{ url('storage'.'/'.$daftar->dokumen) }}" class="btn btn-success py-2">Unduh Dokumen</a> --}}
                            <a href="{{ route('downloadfile', $daftar->id) }}" class="btn btn-success py-2">Unduh Dokumen</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
          </div>

      <div class="row mt-4">
        @if(!count($list) > 0)
            <div class="col-xl-12 d-flex justify-content-center">
                 <h3>Tidak ada Data</h3>
          </div>
        @endif
          <div class="col-xl-12 d-flex justify-content-center">
                 {{ $list->links('vendor.pagination.custom') }}
          </div>
      </div>


{{--     <hr> --}}
    
@endsection