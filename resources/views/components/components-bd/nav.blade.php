<nav>
  <ul id="navigation">
      <li><a href="index-2.html">home</a></li>
      <li><a href="Listings.html">Listings</a></li>
      <li><a href="#">pages <i class="ti-angle-down"></i></a>
          <ul class="submenu">
              <li><a href="elements.html">elements</a></li>
              <li><a href="single_listings.html">Single listing</a></li>
          </ul>
      </li>
      <li><a href="about.html">about</a></li>
      <li><a href="#">blog <i class="ti-angle-down"></i></a>
          <ul class="submenu">
              <li><a href="blog.html">blog</a></li>
              <li><a href="single-blog.html">single-blog</a></li>
          </ul>
      </li>
      <li><a href="contact.html">Contact</a></li>
  </ul>
</nav>