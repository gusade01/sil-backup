<!doctype html>
<html class="no-js" lang="zxx">

<!-- Mirrored from colorlib.com/preview/theme/listingo/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 13 Feb 2020 10:55:29 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>SIL - @yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/x-icon" href="front/img/favicon.png">


    <link rel="stylesheet" href="{{asset('front/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/gijgo.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/slick.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/slicknav.css')}}">
    <link rel="stylesheet" href="../../../../../../ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="{{asset('front/css/style.css')}}">


    <!-- Bootstrap core CSS -->
    <link href="{{asset('front/portal1/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('front/portal1/skins/eden.css')}}" rel="stylesheet">
    <link href="{{asset('front/portal1/hover/hover.css')}}" rel="stylesheet">
    <link href="{{asset('front/portal1/hover/hover-min.css')}}" rel="stylesheet">
    <link href="{{asset('front/portal1/css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/portal1/css/icons/icons.css')}}" rel="stylesheet">
    <link href="{{asset('front/portal1//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{asset('front/portal1/style.css')}}" rel="stylesheet">

    <style type='text/css'>
        .element,
        html {
            scrollbar-width: none overflow-y: hidden;
        }

        html::-webkit-scrollbar,
        .element::-webkit-scrollbar {
            display: none
        }
    </style>

</head>


<body>

{{--     @include('components.nav1') --}}

{{--     @include('components.header1') --}}

    @yield('content')

{{--     @include('components.footer1')
 --}}

    <script src="../../../../../cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" type="4a7aae786c8f562f2888f2a6-text/javascript"> </script>

    <script src="{{asset('front/js/vendor/modernizr-3.5.0.min.js')}}"></script>
    <script src="{{asset('front/js/vendor/jquery-1.12.4.min.js')}}"></script>
    <script src="{{asset('front/js/popper.min.js')}}"></script>
    <script src="{{asset('front/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('front/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('front/js/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('front/js/ajax-form.js')}}"></script>
    <script src="{{asset('front/js/waypoints.min.js')}}"></script>
    <script src="{{asset('front/js/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('front/js/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{asset('front/js/scrollIt.js')}}"></script>
    <script src="{{asset('front/js/jquery.scrollUp.min.js')}}"></script>
    <script src="{{asset('front/js/wow.min.js')}}"></script>
    <script src="{{asset('front/js/nice-select.min.js')}}"></script>
    <script src="{{asset('front/js/jquery.slicknav.min.js')}}"></script>
    <script src="{{asset('front/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('front/js/plugins.js')}}"></script>

    <script src="{{asset('front/js/slick.min.js')}}"></script>

    <script src="{{asset('front/js/contact.js')}}"></script>
    <script src="{{asset('front/js/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{asset('front/js/jquery.form.js')}}"></script>
    <script src="{{asset('front/js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('front/js/mail-script.js')}}"></script>
    <script src="{{asset('front/js/main.js')}}"></script>
    <script type="4a7aae786c8f562f2888f2a6-text/javascript">
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-23581568-13');
    </script>
    <script src="../../../../ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="4a7aae786c8f562f2888f2a6-|49" defer=""></script>

    </body>

<!-- Mirrored from colorlib.com/preview/theme/listingo/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 13 Feb 2020 10:55:50 GMT -->
</html>