<!doctype html>
<html class="no-js" lang="zxx">

<!-- Mirrored from colorlib.com/preview/theme/listingo/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 13 Feb 2020 10:55:29 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>SIL - @yield('title')</title>

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap core CSS -->
  <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">

    <link rel="stylesheet" href="{{asset('css/Landing/css/bank-data.css')}}">
    <link rel="stylesheet" href="{{asset('css/Landing/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/Landing/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/Landing/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/Landing/css/style2.css')}}">
    <link rel="stylesheet" href="{{asset('css/Landing/css/bootstrap-select.min.css')}}">


</head>

<body>

{{-- 	@include('components.components-bd.nav') --}}

{{--   @include('components.components-bd.header') --}}

  @yield('content')

{{--   @include('components.components-bd.footer') --}}
  
     <!-- Bootstrap core JavaScript -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="{{asset('css/Landing/js/popper.min.js')}}"></script>
    <script src="{{asset('css/Landing/js/bootstrap.min.js')}}"></script>

    <script src="{{asset('css/Landing/js/bootstrap-select.min.js')}}" ></script>

    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-23581568-13');

    </script>

    <script>
        $(document).ready(function(){
          $("#myBtn").click(function(){
            $("#myModal").modal({backdrop: false});
          });
        });
    </script>

    <script>
        $(document).ready(function(){
            $("#form").hide();
            $("#cariAll").click(function(){
                $("#form").slideToggle();
            });
        });
    </script>


</body>

</html>